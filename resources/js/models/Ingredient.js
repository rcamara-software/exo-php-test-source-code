export class Ingredient {
    constructor(name, price) {
        this.name = name;
        this.price = price;
    }

    validate() {
        if (this.name === "") {
            return false;
        }

        try {
            parseFloat(this.price)
        } catch {
            return false;
        }

        return true;
    }

    save() {
        return new Promise((resolve, reject) => {

            if (this.validate()) {
                return axios.post("/api/save_ingredient", {name: this.name, price: this.price})
                    .then(function (response) {
                        if (response.data.status === 'ok') {
                            resolve(true);
                        } else {
                            reject(false);
                        }
                    })
                    .catch(function (error) {
                        console.error(error);
                        reject(false);
                    });
            } else {
                reject(false);
            }
        });
    }

    static getAll() {
        return axios.post("/api/get_all_ingredients", {name: this.name, price: this.price})
            .then(function (response) {
                if (response.data.status === 'ok') {
                    return response.data.ingredients;
                } else {
                    return false;
                }
            })
            .catch(function (error) {
                console.error(error);
                return false;
            });
    }
}