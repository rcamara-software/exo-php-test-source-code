<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    /**
     * The ingredients that belong to the pizza.
     */
    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class, 'pizza_ingredient');
    }

    public function calculateSellingPrice()
    {
        // Calculate Selling Price (it comes from view BUT
        // I prefer to recalculate in backend)
        $ingredients = $this->ingredients()->get()->toArray();
        $prices = array_map(function ($model) {
            return $model["price"];
        }, $ingredients);

        $sum_ingredients = array_sum($prices);
        $total = $sum_ingredients + ($sum_ingredients * 0.5);

        $this->selling_price = $total;
        $this->save();
    }
}
