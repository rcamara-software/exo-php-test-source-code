import Vue from 'vue'
import VueRouter from 'vue-router'

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Vue.use(VueRouter);

import App from './components/App'
import Home from "./components/Home"
import CreatePizza from "./components/CreatePizza"
import CreateIngredient from "./components/CreateIngredient"

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            props: {}
        },
        {
            path: '/create_pizza',
            name: 'create_pizza',
            component: CreatePizza,
            props: true,
        },
        {
            path: '/create_ingredients',
            name: 'create_ingredient',
            component: CreateIngredient,
            props: {}
        },
    ]
});

const app = new Vue({
    el: '#app',
    components: {App},
    router,
});