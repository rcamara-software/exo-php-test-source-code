<?php

namespace App\Http\Controllers;

use App\Ingredient;
use App\Pizza;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function savePizza(Request $request)
    {
        $id = $request->get('id', 0);
        if ($id === 0) {
            $pizza = new Pizza();
        } else {
            $pizza = Pizza::whereId($id)->first();
        }
        $pizza->name = $request->get('name');
        $ingredients = $request->get('ingredients');

        $result = $pizza->save();

        $pizza->ingredients()->sync($ingredients);
        $pizza->calculateSellingPrice();

        return response()->json([
            "status" => ($result ? "ok" : "ko")
        ]);
    }

    public function saveIngredient(Request $request)
    {
        $ingredient = new Ingredient();
        $ingredient->name = $request->get('name');
        $ingredient->price = $request->get('price');

        $result = $ingredient->save();

        return response()->json([
            "status" => ($result ? "ok" : "ko")
        ]);
    }

    public function getAllIngredients()
    {
        $ingredients = Ingredient::all();

        return response()->json([
            "status" => (count($ingredients) > 0 ? "ok" : "ko"),
            "ingredients" => $ingredients
        ]);
    }

    public function getAllPizzas() {
        $pizzas = Pizza::with("ingredients")->get();

        return response()->json([
            "status" => (count($pizzas) > 0 ? "ok" : "ko"),
            "pizzas" => $pizzas
        ]);
    }
}
