export class Pizza {
    constructor(id, name, ingredients = []) {
        this.id = id;
        this.name = name;
        this.ingredients = ingredients
    }

    validate() {
        if (this.name === "") {
            return false;
        }

        if (this.ingredients.length === 0) {
            return false;
        }

        return true;
    }

    save() {
        return new Promise((resolve, reject) => {
            if (this.validate()) {
                return axios.post("/api/save_pizza", {id: this.id, name: this.name, ingredients: this.ingredients})
                    .then(function (response) {
                        if (response.data.status === 'ok') {
                            resolve(true);
                        } else {
                            reject(false);
                        }
                    })
                    .catch(function (error) {
                        console.error(error);
                        reject(false);
                    });
            } else {
                reject(false);
            }
        });
    }

    static getAll() {
        return axios.post("/api/get_all_pizzas", {name: this.name, price: this.price})
            .then(function (response) {
                if (response.data.status === 'ok') {
                    return response.data.pizzas;
                } else {
                    return false;
                }
            })
            .catch(function (error) {
                console.error(error);
                return false;
            });
    }
}