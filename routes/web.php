<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{any}', function () {
    return view('welcome');
})->where('any', '^(?!api\/)[\/\w\.-]*');

Route::post('/api/save_pizza', 'ApplicationController@savePizza');
Route::post('/api/save_ingredient', 'ApplicationController@saveIngredient');
Route::post('/api/get_all_ingredients', 'ApplicationController@getAllIngredients');
Route::post('/api/get_all_pizzas', 'ApplicationController@getAllPizzas');